#!/bin/bash

#Replace this path if your rider installation is somewhere else
RiderPath="/c/Program\ Files/JetBrains/JetBrains\ Rider\ 2019.2.2/bin/rider64.exe" 
ProjectPath="$1"

for word in $(find . *.sln)
do
    Solution=$word
done

eval $RiderPath "$ProjectPath/$Solution" &