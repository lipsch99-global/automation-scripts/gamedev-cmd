#!/bin/bash

ProjectPath="$1"
line=$(head -n 1 $1/ProjectSettings/ProjectVersion.txt)
UnityVersion=${line:17:10}
#Replace this path if your unity hub installation is somewhere else
UnityPath="/c/Program\ Files/Unity/Hub/Editor/$UnityVersion/Editor/Unity.exe" 
eval $UnityPath -projectPath $ProjectPath &

