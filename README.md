# GameDev CMD
These commands are used to open my common development programs from the command line in a simple fashion.

## Installation
To use these you only need to put them into your _Path_.
If you want to use them as `rider .` instead of `rider.sh .` just rename the files.

## Usage
### Rider
`rider <project directory>`
This will search for a .sln in the specified directory.
I used in the project folder like this: `rider .`

To change the rider installation folder check out the source file `rider.sh`.
### Unity
`unity <project directory>`
This will search for the unity version in the project and open the correct unity.exe

To change the unity installation folder check out the source file `unity.sh`.
